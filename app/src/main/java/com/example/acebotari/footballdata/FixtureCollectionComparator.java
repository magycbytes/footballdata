package com.example.acebotari.footballdata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.acebotari.footballdata.database.FootballDatabaseException;
import com.example.acebotari.footballdata.database.FootballSqlHelper;
import com.example.acebotari.footballdata.database.fixtures.CrudFixtures;
import com.example.acebotari.footballdata.football.Fixture;

import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/28/16.
 */
public class FixtureCollectionComparator {
    private List<Fixture> mAvailableFixtures;
    private SQLiteDatabase mSQLiteDatabase;
    private int mInitialFixtureSize;

    public FixtureCollectionComparator(List<Fixture> availableFixtures, Context context) {
        mAvailableFixtures = availableFixtures;
        mInitialFixtureSize = mAvailableFixtures.size();

        mSQLiteDatabase = new FootballSqlHelper(context).getWritableDatabase();
    }

    public void checkForNewFixtures(List<Fixture> fixtureList) {
        for (int i = 0; i < fixtureList.size(); ++i) {
            if (isFixtureInDatabase(fixtureList.get(i))) {
                continue;
            }
            mAvailableFixtures.add(fixtureList.get(i));
            saveInDatabase(fixtureList.get(i));
        }
    }

    public boolean isNewDataAdded() {
        return mInitialFixtureSize != mAvailableFixtures.size();
    }

    public List<Fixture> getAvailableFixtures() {
        return mAvailableFixtures;
    }

    private boolean isFixtureInDatabase(Fixture newFixture) {
        for (int j = 0; j < mAvailableFixtures.size(); ++j) {
            if (mAvailableFixtures.get(j).getDate().equals(newFixture.getDate())
                    &&
                    mAvailableFixtures.get(j).getHomeTeamName().equals(newFixture.getHomeTeamName())
                    &&
                    mAvailableFixtures.get(j).getAwayTeamName().equals(newFixture.getAwayTeamName())) {
                return true;
            }
        }
        return false;
    }

    private void saveInDatabase(Fixture fixture) {
        try {
            CrudFixtures.save(fixture, mSQLiteDatabase);
        } catch (FootballDatabaseException e) {
            Log.e(this.toString(), "Error at saving in database");
        }
    }
}
