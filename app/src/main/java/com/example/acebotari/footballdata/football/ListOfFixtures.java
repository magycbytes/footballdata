package com.example.acebotari.footballdata.football;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/25/16.
 */
public class ListOfFixtures {
    @SerializedName("count")
    private Integer mCount;
    @SerializedName("fixtures")
    private List<Fixture> mFixtures;

    public List<Fixture> getFixtures() {
        return mFixtures;
    }

    public void setFixtures(List<Fixture> fixtures) {
        this.mFixtures = fixtures;
    }

    public Integer getCount() {
        return mCount;
    }

    public void setCount(Integer count) {
        this.mCount = count;
    }
}
