package com.example.acebotari.footballdata.callbacks;

import android.content.Context;
import android.widget.Toast;

import com.example.acebotari.footballdata.FixtureAdapterPresenter;
import com.example.acebotari.footballdata.FixtureCollectionComparator;
import com.example.acebotari.footballdata.football.Fixture;
import com.example.acebotari.footballdata.football.ListOfFixtures;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class LeagueFixtureCallback implements Callback<ListOfFixtures> {

    private final Context mContext;
    private FixtureAdapterPresenter mFixtureAdapterPresenter;
    private int mLeagueId;

    public LeagueFixtureCallback(Context context, FixtureAdapterPresenter fixturePresenter, int leagueId) {
        mContext = context;
        mFixtureAdapterPresenter = fixturePresenter;
        mLeagueId = leagueId;
    }

    @Override
    public void onResponse(Call<ListOfFixtures> call, Response<ListOfFixtures> response) {
        ListOfFixtures listOfFixtures = response.body();
        if (listOfFixtures == null || listOfFixtures.getCount() < 1) {
            Toast.makeText(mContext, "Server response: code " + response.code(), Toast.LENGTH_SHORT).show();
            return;
        }

        List<Fixture> fixtures = listOfFixtures.getFixtures();
        appendSeasonId(fixtures);

        FixtureCollectionComparator collectionComparator = new FixtureCollectionComparator(mFixtureAdapterPresenter.getFixtures(), mContext);
        collectionComparator.checkForNewFixtures(fixtures);

        if (collectionComparator.isNewDataAdded()) {
            mFixtureAdapterPresenter.showNewSetOfFixtures(collectionComparator.getAvailableFixtures());
        } else {
            Toast.makeText(mContext, "No new data found", Toast.LENGTH_SHORT).show();
        }

        mFixtureAdapterPresenter.stopRefreshingLayout();
    }

    @Override
    public void onFailure(Call<ListOfFixtures> call, Throwable t) {
        Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
        mFixtureAdapterPresenter.stopRefreshingLayout();
    }

    private void appendSeasonId(List<Fixture> targetFixtures) {
        for (Fixture fixture : targetFixtures) {
            fixture.setSoccerSeasonId(mLeagueId);
        }
    }
}
