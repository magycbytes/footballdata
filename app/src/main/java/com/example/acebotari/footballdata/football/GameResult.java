package com.example.acebotari.footballdata.football;

/**
 * Created by Alexandru Cebotari on 4/25/16.
 */
public class GameResult {
    private Integer goalsHomeTeam;
    private Integer goalsAwayTeam;

    public GameResult() {

    }

    public GameResult(Integer goalsHomeTeam, Integer goalsAwayTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
        this.goalsAwayTeam = goalsAwayTeam;
    }

    public Integer getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public void setGoalsHomeTeam(Integer goalsHomeTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
    }

    public Integer getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public void setGoalsAwayTeam(Integer goalsAwayTeam) {
        this.goalsAwayTeam = goalsAwayTeam;
    }
}
