package com.example.acebotari.footballdata;

import junit.framework.Assert;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    @Test
    public void addition_isCorrect() throws Exception {
        SimpleDateFormat format = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String date = "2015-08-09";
        String date2 = "2015-08-08";

        Date transformedDate = format.parse(date);
        Date newDate = format.parse(date2);

        boolean result = transformedDate.equals(newDate);

        System.out.println(result);

        Assert.assertTrue(transformedDate.equals(newDate));

    }
}