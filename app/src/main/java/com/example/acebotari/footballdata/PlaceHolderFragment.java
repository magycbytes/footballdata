package com.example.acebotari.footballdata;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceHolderFragment extends Fragment {

    public static Fragment newInstance(int sectionNumber) {

        switch (sectionNumber) {
            case 0:
                return new LeaguesFragment();
            case 1:
                return new FixtureFragment();
            default:
                return new PlaceHolderFragment();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_content, container, false);
    }
}
