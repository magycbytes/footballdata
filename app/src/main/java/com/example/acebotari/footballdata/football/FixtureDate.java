package com.example.acebotari.footballdata.football;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class FixtureDate implements Comparable<FixtureDate> {
    private Date mDateAndTime;
    private Date mDate;

    public FixtureDate(String date) throws ParseException {
        extractDate(date);
        extractDateAndTime(date);
    }

    public Date getDate() {
        return mDate;
    }

    private void extractDateAndTime(String original) throws ParseException {
        SimpleDateFormat dateAndTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault());
        dateAndTimeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        mDateAndTime = dateAndTimeFormat.parse(original);
    }

    private void extractDate(String original) throws ParseException {
        String dateSubString = getDateSubString(original);
        SimpleDateFormat justDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        mDate = justDateFormat.parse(dateSubString);
    }

    private static String getDateSubString(String original) throws ParseException {
        int index = original.indexOf('T');
        if (index == -1) {
            throw new ParseException("Not found T in string", -1);
        }
        return original.substring(0, index);
    }

    @Override
    public int compareTo(@NonNull FixtureDate another) {
        int dateCompareResult = mDate.compareTo(another.mDate);
        if (dateCompareResult == 0) {
            return mDateAndTime.compareTo(another.mDateAndTime);
        }
        return dateCompareResult;
    }

    public String getTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDateAndTime);

        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        return timeFormat.format(mDateAndTime);
    }
}
