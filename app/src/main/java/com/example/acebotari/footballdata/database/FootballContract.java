package com.example.acebotari.footballdata.database;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class FootballContract {
    public abstract static class LeaguesTable {
        public static final String TABLE_NAME = "Leagues";
        public static final String LEAGUE_ID = "league_id";
        public static final String LEAGUE_NAME = "league_name";
        public static final String LEAGUE_ACRONYM = "league_acronym";
        public static final String LEAGUE_YEAR = "league_year";
        public static final String LAST_UPDATED = "updated_data";
    }

    public abstract static class Fixtures {
        public static final String TABLE_NAME = "fixtures";
        public static final String FIXTURE_ID = "fixture_id";
        public static final String FIXTURE_DATE = "fixture_date";
        public static final String HOME_TEAM_NAME = "home_team_name";
        public static final String HOME_TEAM_ID = "home_team_id";
        public static final String AWAY_TEAM_NAME = "away_team_name";
        public static final String AWAY_TEAM_ID = "away_team_id";
        public static final String HOME_TEAM_GOALS = "home_team_goals";
        public static final String AWAY_TEAM_GOALS = "away_team_goals";
    }
}
