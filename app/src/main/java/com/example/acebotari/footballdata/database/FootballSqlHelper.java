package com.example.acebotari.footballdata.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class FootballSqlHelper extends SQLiteOpenHelper {
    private final Context mContext;
    private static final String DATABASE_NAME = "FootballData.db";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TABLE_LEAGUES =
            " CREATE TABLE " + FootballContract.LeaguesTable.TABLE_NAME + " (" +
                    FootballContract.LeaguesTable.LEAGUE_ID + " INT, " +
                    FootballContract.LeaguesTable.LEAGUE_NAME + " VARCHAR(128), " +
                    FootballContract.LeaguesTable.LEAGUE_ACRONYM + " VARCHAR(10), " +
                    FootballContract.LeaguesTable.LEAGUE_YEAR + " INT, " +
                    FootballContract.LeaguesTable.LAST_UPDATED + " VARCHAR(32) );";

    private static final String CREATE_TABLE_FIXTURES =
            "CREATE TABLE " + FootballContract.Fixtures.TABLE_NAME + " (" +
                    FootballContract.Fixtures.FIXTURE_ID + " INT, " +
                    FootballContract.LeaguesTable.LEAGUE_ID + " INT, " +
                    FootballContract.Fixtures.FIXTURE_DATE + " VARCHAR(64), " +
                    FootballContract.Fixtures.HOME_TEAM_NAME + " VARCHAR(64), " +
                    FootballContract.Fixtures.HOME_TEAM_ID + " INT, " +
                    FootballContract.Fixtures.AWAY_TEAM_NAME + " VARCHAR(64), " +
                    FootballContract.Fixtures.AWAY_TEAM_ID + " INT, " +
                    FootballContract.Fixtures.HOME_TEAM_GOALS + " INT, " +
                    FootballContract.Fixtures.AWAY_TEAM_GOALS + " INT );";


    public FootballSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LEAGUES);
        db.execSQL(CREATE_TABLE_FIXTURES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mContext.deleteDatabase(DATABASE_NAME);
    }
}
