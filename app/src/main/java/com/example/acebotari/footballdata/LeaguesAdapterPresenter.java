package com.example.acebotari.footballdata;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Alexandru Cebotari on 4/28/16.
 */
public class LeaguesAdapterPresenter implements OnLeagueClickListener {

    private final SharedPreferences mSharedPreferences;
    private LeaguesAdapter mLeaguesAdapter;

    public LeaguesAdapterPresenter(Context context, LeaguesAdapter leaguesAdapter) {
        mLeaguesAdapter = leaguesAdapter;
        mSharedPreferences = context.getSharedPreferences(ContentActivity.FOOTBALL_PREF, Context.MODE_PRIVATE);

        leaguesAdapter.setSelectedLeagueId(mSharedPreferences.getInt(ContentActivity.LEAGUE_SELECT, -1));
        leaguesAdapter.setClickListener(this);
    }


    @Override
    public void clickedOnLeague(int itemPosition) {
        mLeaguesAdapter.setSelectedItem(itemPosition);
        saveNewSelectedLeagueId(mLeaguesAdapter.getSelectedLeagueId());
        mLeaguesAdapter.notifyDataSetChanged();
    }

    private void saveNewSelectedLeagueId(int selectedLeagueId) {
        mSharedPreferences.edit().putInt(ContentActivity.LEAGUE_SELECT, selectedLeagueId).apply();
    }
}
