package com.example.acebotari.footballdata;

import com.example.acebotari.footballdata.football.Fixture;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class FixturesAdapter {
    private final SimpleDateFormat mSimpleDateFormat;
    private final Map<Integer, Object> adapterFixtures;
    private int insertPosition;
    private final List<Fixture> mFixtures;

    public FixturesAdapter(List<Fixture> fixtures) {
        mSimpleDateFormat = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
        mFixtures = fixtures;
        adapterFixtures = new Hashtable<>(fixtures.size());
        insertPosition = 0;
    }

    public Map<Integer, Object> getAdapter() {
        Collections.sort(mFixtures, Collections.<Fixture>reverseOrder());

        if (mFixtures.size() == 0) {
            return adapterFixtures;
        }

        int insertedFixture = 0;
        do {
            insertFixtureDate(insertedFixture);

            insertedFixture += foundFixtureForSameDay(insertedFixture);
        } while (insertedFixture < mFixtures.size());

        return adapterFixtures;
    }

    private void insertFixtureDate(int fixturePosition) {
        Date date = mFixtures.get(fixturePosition).getFixtureDate().getDate();
        String dateToShow = mSimpleDateFormat.format(date);
        adapterFixtures.put(insertPosition++, dateToShow);
    }

    private int foundFixtureForSameDay(int start) {
        Fixture startFixture = mFixtures.get(start);
        adapterFixtures.put(insertPosition++, startFixture);

        int insertItemsCount = 1;

        for (int i = start + 1; i < mFixtures.size(); ++i) {
            Date fixtureDate = startFixture.getFixtureDate().getDate();
            Date compareDate = mFixtures.get(i).getFixtureDate().getDate();
            int compareResult = fixtureDate.compareTo(compareDate);
            if (compareResult == 0) {
                insertItemsCount++;
                adapterFixtures.put(insertPosition++, mFixtures.get(i));
            } else {
                break;
            }
        }
        return insertItemsCount;
    }
}
