package com.example.acebotari.footballdata;

import com.example.acebotari.footballdata.football.Fixture;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Alexandru Cebotari on 4/28/16.
 */
public class NextFixtureCalculator {

    public static int getFixtureToScrollPosition(Map<Integer, Object> adapter) {
        Date currentDate = Calendar.getInstance().getTime();

        return getPositionOfItemToScroll(adapter, currentDate);
    }

    private static int getPositionOfItemToScroll(Map<Integer, Object> adapter, Date currentDate) {
        for (int i = 0; i < adapter.size(); ++i) {
            if (adapter.get(i) instanceof String) {
                continue;
            }

            Fixture fixture = (Fixture) adapter.get(i);
            Date fixtureDate = fixture.getFixtureDate().getDate();

            int compareResult = currentDate.compareTo(fixtureDate);
            if (compareResult > -1) {
                return i;
            }
        }
        return 0;
    }
}
