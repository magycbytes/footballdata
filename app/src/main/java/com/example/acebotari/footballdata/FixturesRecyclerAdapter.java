package com.example.acebotari.footballdata;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.acebotari.footballdata.football.Fixture;

import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
public class FixturesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Map<Integer, Object> mFixtures;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;

    public FixturesRecyclerAdapter(Map<Integer, Object> fixtures) {
        mFixtures = fixtures;
    }

    @Override
    public int getItemViewType(int position) {
        if (mFixtures.get(position) instanceof Fixture) {
            return TYPE_ITEM;
        } else {
            return TYPE_SEPARATOR;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View resultView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_fixture, parent, false);
            return new ViewStoreFixture(resultView);
        }
        View resultView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_fixture_date, parent, false);
        return new ViewStoreDate(resultView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewStoreFixture) {
            ViewStoreFixture viewStoreFixture = (ViewStoreFixture) holder;
            Fixture currentFixture = (Fixture) mFixtures.get(position);

            viewStoreFixture.mTextHomeTeamName.setText(currentFixture.getHomeTeamName());
            viewStoreFixture.mTextAwayTeamName.setText(currentFixture.getAwayTeamName());

            showHomeGoals(viewStoreFixture, currentFixture);
            showAwayGoals(viewStoreFixture, currentFixture);

            viewStoreFixture.mTextStartTime.setText(currentFixture.getFixtureDate().getTime());
        } else {
            ViewStoreDate viewStoreDate = (ViewStoreDate) holder;
            viewStoreDate.mTextFixtureDate.setText((String) mFixtures.get(position));
        }
    }

    private void showHomeGoals(ViewStoreFixture viewStoreFixture, Fixture currentFixture) {
        Integer goalsHomeTeam = currentFixture.getGameResult().getGoalsHomeTeam();
        String homeGoals = "?";
        if (goalsHomeTeam != null && goalsHomeTeam != -1) {
            homeGoals = String.valueOf(goalsHomeTeam);
        }
        viewStoreFixture.mTextHomeTeamGoals.setText(homeGoals);
    }

    private void showAwayGoals(ViewStoreFixture viewStoreFixture, Fixture currentFixture) {
        Integer goalsAwayTeam = currentFixture.getGameResult().getGoalsAwayTeam();
        String awayGoals = "?";
        if (goalsAwayTeam != null && goalsAwayTeam != -1) {
            awayGoals = String.valueOf(goalsAwayTeam);
        }
        viewStoreFixture.mTextAwayTeamGoals.setText(awayGoals);
    }

    public Map<Integer, Object> getFixtures() {
        return mFixtures;
    }

    @Override
    public int getItemCount() {
        return mFixtures.size();
    }

    static class ViewStoreFixture extends RecyclerView.ViewHolder {

        @Bind(R.id.textHomeTeamName)
        TextView mTextHomeTeamName;
        @Bind(R.id.textAwayTeamName)
        TextView mTextAwayTeamName;
        @Bind(R.id.textHomeTeamGoals)
        TextView mTextHomeTeamGoals;
        @Bind(R.id.textAwayTeamGoals)
        TextView mTextAwayTeamGoals;
        @Bind(R.id.textStartTime)
        TextView mTextStartTime;

        public ViewStoreFixture(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    static class ViewStoreDate extends RecyclerView.ViewHolder {

        @Bind(R.id.textFixtureDate)
        TextView mTextFixtureDate;

        public ViewStoreDate(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
