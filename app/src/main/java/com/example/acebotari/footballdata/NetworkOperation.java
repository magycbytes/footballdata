package com.example.acebotari.footballdata;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.example.acebotari.footballdata.callbacks.LeagueFixtureCallback;
import com.example.acebotari.footballdata.callbacks.LeaguesCallback;
import com.example.acebotari.footballdata.football.AuthenticationInterceptor;
import com.example.acebotari.footballdata.football.DataServiceFootball;
import com.example.acebotari.footballdata.football.LeagueDescription;
import com.example.acebotari.footballdata.football.ListOfFixtures;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
class NetworkOperation {
    private static final String BASE_URL = "http://api.football-data.org";
    private final Context mContext;

    public NetworkOperation(Context context) {
        mContext = context;
    }

    public void updateLeagues(List<LeagueDescription> availableLeagues, TextView textNoItems, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new AuthenticationInterceptor()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DataServiceFootball dataServiceFootball = retrofit.create(DataServiceFootball.class);
        Call<List<LeagueDescription>> leagues = dataServiceFootball.getAllLeagues();
        leagues.enqueue(new LeaguesCallback(mContext, availableLeagues, recyclerView, swipeRefreshLayout, textNoItems));
    }

    public void updateFixtures(int seasonId, FixtureAdapterPresenter adapterPresenter) {
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new AuthenticationInterceptor()).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        DataServiceFootball dataServiceFootball = retrofit.create(DataServiceFootball.class);
        Call<ListOfFixtures> leagueFixture = dataServiceFootball.getLeagueFixture(seasonId);
        leagueFixture.enqueue(new LeagueFixtureCallback(mContext, adapterPresenter, seasonId));
    }
}
