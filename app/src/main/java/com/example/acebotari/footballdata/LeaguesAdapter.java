package com.example.acebotari.footballdata;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.acebotari.footballdata.football.LeagueDescription;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
public class LeaguesAdapter extends RecyclerView.Adapter<LeaguesAdapter.ViewStore> {
    private final List<LeagueDescription> mLeagues;
    private final Context mContext;
    private int mSelectedLeagueId;
    private OnLeagueClickListener mClickListener;

    public LeaguesAdapter(List<LeagueDescription> leagues, Context context) {
        mLeagues = leagues;
        mContext = context;
    }

    @Override
    public ViewStore onCreateViewHolder(ViewGroup parent, int viewType) {
        View resultView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_leagues, parent, false);
        return new ViewStore(resultView, mClickListener);
    }

    @Override
    public void onBindViewHolder(ViewStore holder, int position) {
        LeagueDescription leagueToShow = mLeagues.get(position);

        changeCardBackground(holder, leagueToShow);

        holder.mTextLeagueName.setText(leagueToShow.getCaption());
        holder.mTextLeagueAcronym.setText(leagueToShow.getAcronym());
    }

    @Override
    public int getItemCount() {
        return mLeagues.size();
    }

    public void setSelectedLeagueId(int selectedLeagueId) {
        mSelectedLeagueId = selectedLeagueId;
    }

    public void setSelectedItem(int itemPosition) {
        mSelectedLeagueId = mLeagues.get(itemPosition).getId();
    }

    public List<LeagueDescription> getLeagues() {
        return mLeagues;
    }

    public int getSelectedLeagueId() {
        return mSelectedLeagueId;
    }

    private void changeCardBackground(ViewStore holder, LeagueDescription leagueToShow) {
        if (leagueToShow.getId() == mSelectedLeagueId) {
            holder.rootView.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorSelectedLeague));
        } else {
            holder.rootView.setCardBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }
    }

    public void setClickListener(OnLeagueClickListener clickListener) {
        mClickListener = clickListener;
    }

    public static class ViewStore extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final OnLeagueClickListener mOnLeagueClickListener;
        @Bind(R.id.textLeagueName)
        public TextView mTextLeagueName;
        @Bind(R.id.textAcronymLeague)
        public TextView mTextLeagueAcronym;

        @Bind(R.id.cardViewLeague)
        public CardView rootView;

        public ViewStore(View itemView, OnLeagueClickListener leagueClickListener) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            mOnLeagueClickListener = leagueClickListener;
        }
        @OnClick(R.id.cardViewLeague)
        @Override
        public void onClick(View v) {
            if (mOnLeagueClickListener == null) {
                return;
            }
            mOnLeagueClickListener.clickedOnLeague(getAdapterPosition());
        }

    }
}
