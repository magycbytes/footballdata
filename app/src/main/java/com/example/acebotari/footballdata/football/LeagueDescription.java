package com.example.acebotari.footballdata.football;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexandru Cebotari on 4/25/16.
 */
public class LeagueDescription implements Comparable<LeagueDescription> {
    private Integer id;
    private String caption;
    @SerializedName("league")
    private String acronym;
    private String year;
    private String lastUpdated;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public int compareTo(@NonNull LeagueDescription another) {
        return caption.compareTo(another.caption);
    }
}
