package com.example.acebotari.footballdata.database.fixtures;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.acebotari.footballdata.database.FootballContract;
import com.example.acebotari.footballdata.database.FootballDatabaseException;
import com.example.acebotari.footballdata.football.Fixture;
import com.example.acebotari.footballdata.football.GameResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
public class CrudFixtures {

    public static void save(Fixture fixture, SQLiteDatabase sqLiteDatabase) throws FootballDatabaseException {
        long insertedId = sqLiteDatabase.insert(FootballContract.Fixtures.TABLE_NAME, null, ValuesMapperFixtures.mapFixture(fixture));
        if (insertedId == -1) {
            throw new FootballDatabaseException();
        }
    }

    @SuppressWarnings("resource")
    public static List<Fixture> getAll(SQLiteDatabase sqLiteDatabase, int leagueId) {
        String selection = FootballContract.LeaguesTable.LEAGUE_ID + " LIKE ?";
        String[] selectionArg = new String[]{String.valueOf(leagueId)};

        Cursor cursor = sqLiteDatabase.query(
                FootballContract.Fixtures.TABLE_NAME, ValuesMapperFixtures.selectColumns(), selection, selectionArg, null, null, null);

        List<Fixture> allFixtures = new ArrayList<>(cursor.getCount());
        if (!cursor.moveToFirst()) {
            cursor.close();
            return allFixtures;
        }

        do {
            allFixtures.add(extractFixtureFromCursor(cursor, leagueId));
        } while (cursor.moveToNext());

        cursor.close();
        return allFixtures;
    }

    private static Fixture extractFixtureFromCursor(Cursor cursor, int leagueId) {
        int fixtureId = cursor.getInt(cursor.getColumnIndex(FootballContract.Fixtures.FIXTURE_ID));
        String date = cursor.getString(cursor.getColumnIndex(FootballContract.Fixtures.FIXTURE_DATE));
        String homeTeamName = cursor.getString(cursor.getColumnIndex(FootballContract.Fixtures.HOME_TEAM_NAME));
        int homeTeamGoals = cursor.getInt(cursor.getColumnIndex(FootballContract.Fixtures.HOME_TEAM_GOALS));
        int homeTeamId = cursor.getInt(cursor.getColumnIndex(FootballContract.Fixtures.HOME_TEAM_ID));
        String awayTeamName = cursor.getString(cursor.getColumnIndex(FootballContract.Fixtures.AWAY_TEAM_NAME));
        int awayTeamId = cursor.getInt(cursor.getColumnIndex(FootballContract.Fixtures.AWAY_TEAM_ID));
        int awayTeamGoals = cursor.getInt(cursor.getColumnIndex(FootballContract.Fixtures.AWAY_TEAM_GOALS));

        return new Fixture(date, -1, homeTeamName, awayTeamName, new GameResult(homeTeamGoals, awayTeamGoals), fixtureId, leagueId, homeTeamId, awayTeamId);
    }
}
