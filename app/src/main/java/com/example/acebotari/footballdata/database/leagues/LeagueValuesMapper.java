package com.example.acebotari.footballdata.database.leagues;

import android.content.ContentValues;

import com.example.acebotari.footballdata.database.FootballContract;
import com.example.acebotari.footballdata.football.LeagueDescription;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
class LeagueValuesMapper {

    public static ContentValues mapLeague(LeagueDescription league) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FootballContract.LeaguesTable.LEAGUE_ID, league.getId());
        contentValues.put(FootballContract.LeaguesTable.LEAGUE_NAME, league.getCaption());
        contentValues.put(FootballContract.LeaguesTable.LEAGUE_ACRONYM, league.getAcronym());
        contentValues.put(FootballContract.LeaguesTable.LEAGUE_YEAR, league.getYear());
        contentValues.put(FootballContract.LeaguesTable.LAST_UPDATED, league.getLastUpdated());

        return contentValues;
    }

    public static String[] selectColumns() {
        return new String[]{
                FootballContract.LeaguesTable.LEAGUE_ID,
                FootballContract.LeaguesTable.LEAGUE_NAME,
                FootballContract.LeaguesTable.LEAGUE_ACRONYM,
                FootballContract.LeaguesTable.LEAGUE_YEAR,
                FootballContract.LeaguesTable.LAST_UPDATED
        };
    }
}
