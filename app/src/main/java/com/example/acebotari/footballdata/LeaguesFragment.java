package com.example.acebotari.footballdata;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.acebotari.footballdata.database.FootballSqlHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
public class LeaguesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @Bind(android.R.id.list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeRefreshLeagues)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.textNoLeagues)
    TextView mTextNoLeagues;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View resultView = inflater.inflate(R.layout.fragment_leagues, container, false);

        ButterKnife.bind(this, resultView);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mSwipeRefreshLayout.setOnRefreshListener(this);

        SQLiteDatabase readableDatabase = new FootballSqlHelper(getContext()).getReadableDatabase();

        LoaderDbLeagues loaderDbLeagues = new LoaderDbLeagues(getContext(), readableDatabase, mRecyclerView, mTextNoLeagues);
        loaderDbLeagues.execute();

        return resultView;
    }

    @Override
    public void onRefresh() {
        LeaguesAdapter adapter = (LeaguesAdapter) mRecyclerView.getAdapter();

        if (adapter == null) {
            Toast.makeText(getContext(), "Loading data from database first", Toast.LENGTH_SHORT).show();
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        }

        NetworkOperation networkOperation = new NetworkOperation(getContext());
        networkOperation.updateLeagues(adapter.getLeagues(), mTextNoLeagues, mSwipeRefreshLayout, mRecyclerView);
    }
}
