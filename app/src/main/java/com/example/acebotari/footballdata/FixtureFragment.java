package com.example.acebotari.footballdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.acebotari.footballdata.database.FootballSqlHelper;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
public class FixtureFragment extends android.support.v4.app.Fragment implements SwipeRefreshLayout.OnRefreshListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private FixtureAdapterPresenter mAdapterPresenter;
    private SharedPreferences mSharedPreferences;

    @Bind(R.id.textNoLeagueSelected)
    TextView mTextNoLeagueSelected;
    @Bind(android.R.id.list)
    RecyclerView mRecyclerView;
    @Bind(R.id.swipeRefreshFixtures)
    SwipeRefreshLayout mSwipeRefreshFixtures;
    private SQLiteDatabase mReadableDatabase;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View resultView = inflater.inflate(R.layout.fragment_fixtures, container, false);
        ButterKnife.bind(this, resultView);

        mSharedPreferences = getContext().getSharedPreferences(ContentActivity.FOOTBALL_PREF, Context.MODE_PRIVATE);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mSwipeRefreshFixtures.setOnRefreshListener(this);

        mReadableDatabase = new FootballSqlHelper(getContext()).getReadableDatabase();
        mAdapterPresenter = new FixtureAdapterPresenter(getContext(), mReadableDatabase, mSwipeRefreshFixtures, mRecyclerView, mTextNoLeagueSelected);

        loadDataFromDatabase();

        return resultView;
    }

    @Override
    public void onRefresh() {
        int selectedLeagueId = getSelectedLeagueId();

        if (selectedLeagueId == -1) {
            Toast.makeText(getContext(), "Not selected league", Toast.LENGTH_SHORT).show();
            mSwipeRefreshFixtures.setRefreshing(false);
            return;
        }

        if (mAdapterPresenter.isAdapterNotSet()) {
            Toast.makeText(getContext(), "Loading data from database first", Toast.LENGTH_SHORT).show();
            mSwipeRefreshFixtures.setRefreshing(false);
            return;
        }

        NetworkOperation networkOperation = new NetworkOperation(getContext());
        networkOperation.updateFixtures(selectedLeagueId, mAdapterPresenter);
    }

    private int getSelectedLeagueId() {
        return mSharedPreferences.getInt(ContentActivity.LEAGUE_SELECT, -1);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(ContentActivity.LEAGUE_SELECT)) {
            loadDataFromDatabase();
        }
    }

    private void loadDataFromDatabase() {
        int selectedLeague = getSelectedLeagueId();
        if (selectedLeague != -1) {
            new LoaderFixturesDb(selectedLeague, mAdapterPresenter, mReadableDatabase).execute();
        }
    }
}
