package com.example.acebotari.footballdata.callbacks;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.acebotari.footballdata.LeaguesAdapter;
import com.example.acebotari.footballdata.LeaguesAdapterPresenter;
import com.example.acebotari.footballdata.database.FootballDatabaseException;
import com.example.acebotari.footballdata.database.FootballSqlHelper;
import com.example.acebotari.footballdata.database.leagues.CrudLeagues;
import com.example.acebotari.footballdata.football.LeagueDescription;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class LeaguesCallback implements Callback<List<LeagueDescription>> {
    private final Context mContext;
    private final List<LeagueDescription> mAvailableLeagues;
    private FootballSqlHelper mSqlHelper;
    private final RecyclerView mRecyclerView;
    private final SwipeRefreshLayout mSwipeRefreshLayout;
    private final TextView mTextNoItems;

    public LeaguesCallback(Context context, List<LeagueDescription> availableLeagues, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, TextView textNoItems) {
        mContext = context;
        mAvailableLeagues = availableLeagues;
        mRecyclerView = recyclerView;
        mSwipeRefreshLayout = swipeRefreshLayout;
        mTextNoItems = textNoItems;
    }

    @Override
    public void onResponse(Call<List<LeagueDescription>> call, Response<List<LeagueDescription>> response) {
        List<LeagueDescription> listOfLeagues = response.body();
        if (listOfLeagues == null || listOfLeagues.size() < 1) {
            Toast.makeText(mContext, "Server response: code " + response.code(), Toast.LENGTH_SHORT).show();
            return;
        }

        int initialLeaguesCnt = mAvailableLeagues.size();
        findNewLeagues(listOfLeagues);

        if (initialLeaguesCnt != mAvailableLeagues.size()) {
            Collections.sort(mAvailableLeagues);
            LeaguesAdapter leaguesAdapter = new LeaguesAdapter(mAvailableLeagues, mContext);
            new LeaguesAdapterPresenter(mContext, leaguesAdapter);
            mRecyclerView.setAdapter(leaguesAdapter);
        } else {
            Toast.makeText(mContext, "No new data found", Toast.LENGTH_SHORT).show();
        }

        if (mAvailableLeagues.size() < 1) {
            mTextNoItems.setVisibility(View.VISIBLE);
        } else {
            mTextNoItems.setVisibility(View.INVISIBLE);
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void findNewLeagues(List<LeagueDescription> listOfLeagues) {
        mSqlHelper = new FootballSqlHelper(mContext);

        for (int i = 0; i < listOfLeagues.size(); ++i) {
            if (isNewLeaguePresentInAvailable(listOfLeagues.get(i))) {
                continue;
            }
            mAvailableLeagues.add(listOfLeagues.get(i));
            saveInDatabase(listOfLeagues.get(i));
        }
    }

    private boolean isNewLeaguePresentInAvailable(LeagueDescription newLeague) {
        for (int j = 0; j < mAvailableLeagues.size(); ++j) {
            if (mAvailableLeagues.get(j).getId().equals(newLeague.getId())
                    &&
                    mAvailableLeagues.get(j).getLastUpdated().equals(newLeague.getLastUpdated())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onFailure(Call<List<LeagueDescription>> call, Throwable t) {
        Toast.makeText(mContext, "Problem at downloading leagues: " + t.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void saveInDatabase(LeagueDescription league) {
        try {
            CrudLeagues.save(league, mSqlHelper.getWritableDatabase());
        } catch (FootballDatabaseException e) {
            Log.e(this.toString(), "Error at saving database");
        }
    }
}
