package com.example.acebotari.footballdata.football;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class AuthenticationInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request modifiedRequest = request.newBuilder().addHeader("X-Auth-Token", "090079f598f24978a8265acaae95467a").build();
        return chain.proceed(modifiedRequest);
    }
}
