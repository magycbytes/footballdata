package com.example.acebotari.footballdata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.acebotari.footballdata.database.leagues.CrudLeagues;
import com.example.acebotari.footballdata.football.LeagueDescription;

import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
class LoaderDbLeagues extends AsyncTask<Void, Void, Void> {

    private final Context mContext;
    private List<LeagueDescription> mAllLeagues;
    private SQLiteDatabase mSQLiteDatabase;

    private final RecyclerView mRecyclerView;
    private final TextView mTextView;

    public LoaderDbLeagues(Context context, SQLiteDatabase sqLiteDatabase, RecyclerView recyclerView, TextView textView) {
        mContext = context;
        mSQLiteDatabase = sqLiteDatabase;
        mRecyclerView = recyclerView;
        mTextView = textView;
    }

    @Override
    protected Void doInBackground(Void... params) {
        mAllLeagues = CrudLeagues.getAllLeagues(mSQLiteDatabase);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        LeaguesAdapter leaguesAdapter = new LeaguesAdapter(mAllLeagues, mContext);
        mRecyclerView.setAdapter(leaguesAdapter);

        new LeaguesAdapterPresenter(mContext, leaguesAdapter);

        if (mAllLeagues.size() < 1) {
            mTextView.setVisibility(View.VISIBLE);
        } else {
            mTextView.setVisibility(View.INVISIBLE);
        }
    }
}
