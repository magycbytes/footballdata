package com.example.acebotari.footballdata;

import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.example.acebotari.footballdata.database.fixtures.CrudFixtures;
import com.example.acebotari.footballdata.football.Fixture;

import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
class LoaderFixturesDb extends AsyncTask<Void, Void, Void> {

    private final int mLeagueId;
    private List<Fixture> mExtractedFixtures;
    private FixtureAdapterPresenter mAdapterPresenter;
    private SQLiteDatabase mSQLiteDatabase;

    public LoaderFixturesDb(int leagueId, FixtureAdapterPresenter adapterPresenter, SQLiteDatabase sqLiteDatabase) {
        mLeagueId = leagueId;
        mAdapterPresenter = adapterPresenter;
        mSQLiteDatabase = sqLiteDatabase;
    }

    @Override
    protected Void doInBackground(Void... params) {
        mExtractedFixtures = CrudFixtures.getAll(mSQLiteDatabase, mLeagueId);
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        mAdapterPresenter.showNewSetOfFixtures(mExtractedFixtures);
    }
}
