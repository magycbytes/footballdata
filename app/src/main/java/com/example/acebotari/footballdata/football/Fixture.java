package com.example.acebotari.footballdata.football;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;

/**
 * Created by Alexandru Cebotari on 4/25/16.
 */
@SuppressWarnings("unused")
public class Fixture implements Comparable<Fixture> {
    @SerializedName("date")
    private String mDate;
    @SerializedName("matchday")
    private int mMatchDay;
    @SerializedName("homeTeamName")
    private String mHomeTeamName;
    @SerializedName("awayTeamName")
    private String mAwayTeamName;
    @SerializedName("result")
    private GameResult mGameResult;
    @SerializedName("id")
    private int mFixtureId;
    @SerializedName("soccerseasonId")
    private int mSoccerSeasonId;
    private FixtureDate mFixtureDate;
    @SerializedName("homeTeamId")
    private Integer mHomeTeamId;
    @SerializedName("awayTeamId")
    private Integer mAwayTeamId;

    public Fixture() {

    }

    public Fixture(String date, int matchDay, String homeTeamName, String awayTeamName, GameResult gameResult, int fixtureId, int soccerSeasonId, int homeTeamId, int awayTeamId) {
        this.mDate = date;
        this.mMatchDay = matchDay;
        this.mHomeTeamName = homeTeamName;
        this.mAwayTeamName = awayTeamName;
        this.mGameResult = gameResult;
        mFixtureId = fixtureId;
        this.mSoccerSeasonId = soccerSeasonId;
        this.mHomeTeamId = homeTeamId;
        this.mAwayTeamId = awayTeamId;
    }

    public int getMatchDay() {
        return mMatchDay;
    }

    public void setMatchDay(int matchDay) {
        this.mMatchDay = matchDay;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        this.mDate = date;
    }

    public String getHomeTeamName() {
        return mHomeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.mHomeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return mAwayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.mAwayTeamName = awayTeamName;
    }

    public GameResult getGameResult() {
        return mGameResult;
    }

    public void setGameResult(GameResult gameResult) {
        this.mGameResult = gameResult;
    }

    public FixtureDate getFixtureDate() {
        if (mFixtureDate == null) {
            calculateDate();
        }
        return mFixtureDate;
    }

    private void calculateDate() {
        try {
            mFixtureDate = new FixtureDate(mDate);
        } catch (ParseException e) {
            Log.e(this.toString(), e.getMessage());
        }
    }

    @Override
    public int compareTo(@NonNull Fixture another) {
        if (mFixtureDate == null) {
            calculateDate();
        }
        if (another.getFixtureDate() == null) {
            another.calculateDate();
        }
        return mFixtureDate.compareTo(another.getFixtureDate());
    }

    public int getId() {
        return mFixtureId;
    }

    public void setId(int id) {
        this.mFixtureId = id;
    }

    public int getSoccerSeasonId() {
        return mSoccerSeasonId;
    }

    public void setSoccerSeasonId(int soccerSeasonId) {
        this.mSoccerSeasonId = soccerSeasonId;
    }

    public Integer getHomeTeamId() {
        return mHomeTeamId;
    }

    public void setHomeTeamId(Integer homeTeamId) {
        this.mHomeTeamId = homeTeamId;
    }

    public Integer getAwayTeamId() {
        return mAwayTeamId;
    }

    public void setAwayTeamId(int awayTeamId) {
        this.mAwayTeamId = awayTeamId;
    }
}
