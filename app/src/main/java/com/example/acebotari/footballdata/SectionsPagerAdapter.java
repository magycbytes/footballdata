package com.example.acebotari.footballdata;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

class SectionsPagerAdapter extends FragmentStatePagerAdapter {
    private final String[] mPageTitles = {"League", "Fixtures", "Table"};
    private final Map<Integer, Fragment> mFragmentMap;

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragmentMap = new HashMap<>(mPageTitles.length);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        mFragmentMap.remove(position);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = PlaceHolderFragment.newInstance(position);
        mFragmentMap.put(position, fragment);
        return fragment;
    }

    @Override
    public int getCount() {
        return mPageTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }

    public Fragment getFragment(int position) {
        return mFragmentMap.get(position);
    }
}