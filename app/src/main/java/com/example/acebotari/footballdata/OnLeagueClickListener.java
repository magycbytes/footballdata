package com.example.acebotari.footballdata;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
interface OnLeagueClickListener {
    void clickedOnLeague(int itemPosition);
}