package com.example.acebotari.footballdata;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.acebotari.footballdata.football.Fixture;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Alexandru Cebotari on 4/28/16.
 */
public class FixtureAdapterPresenter implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int DELAY_SCROLLING_MILLISECONDS = 300;

    private FixturesRecyclerAdapter mRecyclerAdapter;
    private SharedPreferences mSharedPreferences;
    private SQLiteDatabase mSQLiteDatabase;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private TextView mTextNoLeagueSelected;

    public FixtureAdapterPresenter(Context context, SQLiteDatabase sqLiteDatabase, SwipeRefreshLayout swipeRefreshLayout, RecyclerView recyclerView, TextView textNoLeagueSelected) {
        mSQLiteDatabase = sqLiteDatabase;
        mSwipeRefreshLayout = swipeRefreshLayout;
        mRecyclerView = recyclerView;
        mTextNoLeagueSelected = textNoLeagueSelected;

        mSharedPreferences = context.getSharedPreferences(ContentActivity.FOOTBALL_PREF, Context.MODE_PRIVATE);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        checkTextNoLeagueSelected();
    }

    public List<Fixture> getFixtures() {
        Map<Integer, Object> mapFixtures = mRecyclerAdapter.getFixtures();

        List<Fixture> fixtures = new ArrayList<>(mapFixtures.size());
        for (int i = 0; i < mapFixtures.size(); ++i) {
            if (mapFixtures.get(i) instanceof Fixture) {
                fixtures.add((Fixture) mapFixtures.get(i));
            }
        }
        return fixtures;
    }

    public void showNewSetOfFixtures(List<Fixture> fixturesList) {
        Collections.sort(fixturesList, Collections.<Fixture>reverseOrder());

        FixturesAdapter fixturesAdapter = new FixturesAdapter(fixturesList);
        Map<Integer, Object> adapter = fixturesAdapter.getAdapter();

        mRecyclerAdapter = new FixturesRecyclerAdapter(adapter);
        mRecyclerView.setAdapter(mRecyclerAdapter);

        moveToIndicatedItemInList(NextFixtureCalculator.getFixtureToScrollPosition(adapter));
    }

    public void stopRefreshingLayout() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public boolean isAdapterNotSet() {
        return mRecyclerAdapter == null;
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.i(this.toString(), "In preference changed");
        if (key.equals(ContentActivity.LEAGUE_SELECT)) {
            int leagueId = getSelectedLeagueId();

            Log.i(this.toString(), "Loading new fixtures, league id: " + leagueId);
            new LoaderFixturesDb(leagueId, this, mSQLiteDatabase).execute();
            checkTextNoLeagueSelected();
        }
    }

    private void moveToIndicatedItemInList(int goToItem) {
        final int finalGoToItem = goToItem;
        mRecyclerView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mRecyclerView.smoothScrollToPosition(finalGoToItem);
            }
        }, DELAY_SCROLLING_MILLISECONDS);
    }

    private void checkTextNoLeagueSelected() {
        if (getSelectedLeagueId() == -1) {
            mTextNoLeagueSelected.setVisibility(View.VISIBLE);
        } else {
            mTextNoLeagueSelected.setVisibility(View.GONE);
        }
    }

    private int getSelectedLeagueId() {
        return mSharedPreferences.getInt(ContentActivity.LEAGUE_SELECT, -1);
    }
}
