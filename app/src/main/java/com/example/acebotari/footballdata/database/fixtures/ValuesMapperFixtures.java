package com.example.acebotari.footballdata.database.fixtures;

import android.content.ContentValues;

import com.example.acebotari.footballdata.database.FootballContract;
import com.example.acebotari.footballdata.football.Fixture;

/**
 * Created by Alexandru Cebotari on 4/27/16.
 */
class ValuesMapperFixtures {

    public static ContentValues mapFixture(Fixture fixture) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(FootballContract.Fixtures.FIXTURE_ID, fixture.getId());
        contentValues.put(FootballContract.LeaguesTable.LEAGUE_ID, fixture.getSoccerSeasonId());
        contentValues.put(FootballContract.Fixtures.FIXTURE_DATE, fixture.getDate());
        contentValues.put(FootballContract.Fixtures.HOME_TEAM_NAME, fixture.getHomeTeamName());

        if (fixture.getGameResult().getGoalsHomeTeam() == null) {
            contentValues.put(FootballContract.Fixtures.HOME_TEAM_GOALS, -1);
        } else {
            contentValues.put(FootballContract.Fixtures.HOME_TEAM_GOALS, fixture.getGameResult().getGoalsHomeTeam());
        }
        contentValues.put(FootballContract.Fixtures.HOME_TEAM_ID, fixture.getHomeTeamId());
        contentValues.put(FootballContract.Fixtures.AWAY_TEAM_NAME, fixture.getAwayTeamName());
        contentValues.put(FootballContract.Fixtures.AWAY_TEAM_ID, fixture.getAwayTeamId());

        if (fixture.getGameResult().getGoalsAwayTeam() == null) {
            contentValues.put(FootballContract.Fixtures.AWAY_TEAM_GOALS, -1);
        } else {
            contentValues.put(FootballContract.Fixtures.AWAY_TEAM_GOALS, fixture.getGameResult().getGoalsAwayTeam());
        }

        return contentValues;
    }

    public static String[] selectColumns() {
        return new String[]{
                FootballContract.Fixtures.FIXTURE_ID,
                FootballContract.LeaguesTable.LEAGUE_ID,
                FootballContract.Fixtures.FIXTURE_DATE,
                FootballContract.Fixtures.HOME_TEAM_NAME,
                FootballContract.Fixtures.HOME_TEAM_GOALS,
                FootballContract.Fixtures.HOME_TEAM_ID,
                FootballContract.Fixtures.AWAY_TEAM_NAME,
                FootballContract.Fixtures.AWAY_TEAM_GOALS,
                FootballContract.Fixtures.AWAY_TEAM_ID,
        };
    }
}
