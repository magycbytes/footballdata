package com.example.acebotari.footballdata.database.leagues;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;

import com.example.acebotari.footballdata.database.FootballContract;
import com.example.acebotari.footballdata.database.FootballDatabaseException;
import com.example.acebotari.footballdata.football.LeagueDescription;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandru Cebotari on 4/26/16.
 */
public class CrudLeagues {

    public static void save(LeagueDescription league, SQLiteDatabase sqLiteDatabase) throws FootballDatabaseException {
        ContentValues contentValues = LeagueValuesMapper.mapLeague(league);

        long insertId = sqLiteDatabase.insert(FootballContract.LeaguesTable.TABLE_NAME, null, contentValues);
        if (insertId == -1) {
            throw new FootballDatabaseException();
        }
    }

    public static List<LeagueDescription> getAllLeagues(SQLiteDatabase sqLiteDatabase) {
        Cursor cursor = sqLiteDatabase.query(FootballContract.LeaguesTable.TABLE_NAME, LeagueValuesMapper.selectColumns(), null, null, null, null, FootballContract.LeaguesTable.LEAGUE_NAME + " ASC");

        List<LeagueDescription> allLeagues = new ArrayList<>(cursor.getCount());
        if (!cursor.moveToFirst()) {
            return allLeagues;
        }
        do {
            LeagueDescription extractedLeague = getLeague(cursor);

            allLeagues.add(extractedLeague);
        } while (cursor.moveToNext());
        return allLeagues;
    }

    @NonNull
    private static LeagueDescription getLeague(Cursor cursor) {
        int leagueYear = cursor.getInt(cursor.getColumnIndex(FootballContract.LeaguesTable.LEAGUE_YEAR));

        LeagueDescription extractedLeague = new LeagueDescription();
        extractedLeague.setId(cursor.getInt(cursor.getColumnIndex(FootballContract.LeaguesTable.LEAGUE_ID)));
        extractedLeague.setCaption(cursor.getString(cursor.getColumnIndex(FootballContract.LeaguesTable.LEAGUE_NAME)));
        extractedLeague.setAcronym(cursor.getString(cursor.getColumnIndex(FootballContract.LeaguesTable.LEAGUE_ACRONYM)));
        extractedLeague.setYear(String.valueOf(leagueYear));
        extractedLeague.setLastUpdated(cursor.getString(cursor.getColumnIndex(FootballContract.LeaguesTable.LAST_UPDATED)));
        return extractedLeague;
    }
}
