package com.example.acebotari.footballdata.football;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alexandru Cebotari on 4/25/16.
 */
public interface DataServiceFootball {

    @GET("/v1/soccerseasons/")
    Call<List<LeagueDescription>> getAllLeagues();

    @GET("/v1/soccerseasons/")
    Call<List<LeagueDescription>> getAllLeagues(@Query("season")int seasonYear);

    @GET("/v1/soccerseasons/{leagueId}/fixtures/")
    Call<ListOfFixtures> getLeagueFixture(@Path("leagueId") int leagueId);
}
